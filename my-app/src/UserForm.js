import React, { useState } from 'react';

const UserForm = () => {
  const [resetDisabled, setResetDisabled] = useState(true);
  const [formState, setFormState] = useState({
    firstName: '',
    lastName: '',
    age: '',
    employed: false,
    favoriteColor: '',
    toppings: [],
    stooge: '',
    notes: ''
  });
  const [errors, setErrors] = useState({});

  const handleSubmit = (event) => {
    event.preventDefault();
    const formIsValid = validateForm();
    if (formIsValid) {
      alert(JSON.stringify(formState));
    } else {
      alert('Please correct the form errors.');
    }
  };

  const validateForm = () => {
    let isValid = true;
    const errorsObj = {};

    // First name validation
    if (!/^[a-zA-Z\s]+$/.test(formState.firstName.trim())) {
      errorsObj.firstName = true;
      isValid = false;
    }

    // Last name validation
    if (!/^[a-zA-Z\s]+$/.test(formState.lastName.trim())) {
      errorsObj.lastName = true;
      isValid = false;
    }

    // Age validation
    if (!/^\d+$/.test(formState.age)) {
      errorsObj.age = true;
      isValid = false;
    }

    // Notes length validation
    if (formState.notes.length > 100) {
      errorsObj.notes = true;
      isValid = false;
    }

    setErrors(errorsObj);
    return isValid;
  };

  const handleReset = () => {
    setFormState({
      firstName: '',
      lastName: '',
      age: '',
      employed: false,
      favoriteColor: '',
      toppings: [],
      stooge: '',
      notes: ''
    });
    setResetDisabled(true);
    setErrors({});
  };

  const handleInputChange = (event) => {
    const { name, value, type, checked } = event.target;
    const newValue = type === 'checkbox' ? checked : value;

    setFormState(prevState => ({
      ...prevState,
      [name]: newValue
    }));
    setResetDisabled(false);
    setErrors(prevErrors => ({ ...prevErrors, [name]: false }));
  };

  return (
    <div className='form-container'>
      <form onSubmit={handleSubmit}>
        <label htmlFor="firstName">First Name:</label>
        <input type="text" id="firstName" name="firstName" value={formState.firstName} onChange={handleInputChange} className={errors.firstName ? 'invalid' : ''} /><br />

        <label htmlFor="lastName">Last Name:</label>
        <input type="text" id="lastName" name="lastName" value={formState.lastName} onChange={handleInputChange} className={errors.lastName ? 'invalid' : ''} /><br />

        <label htmlFor="age">Age:</label>
        <input type="number" id="age" name="age" value={formState.age} onChange={handleInputChange} className={errors.age ? 'invalid' : ''} /><br />

        <label htmlFor="employed">Employed:</label>
        <input type="checkbox" id="employed" name="employed" checked={formState.employed} onChange={handleInputChange} /><br />

        <label htmlFor="favoriteColor">Favorite Color:</label>
        <select id="favoriteColor" name="favoriteColor" value={formState.favoriteColor} onChange={handleInputChange}>
          <option value="red">Red</option>
          <option value="blue">Blue</option>
          <option value="green">Green</option>
          <option value="yellow">Yellow</option>
          <option value="orange">Orange</option>
        </select><br />

        <label>Choose Toppings:</label><br />

        <div className='toppings-container'>
          <input type="checkbox" id="topping1" name="toppings" value="cheese" onChange={handleInputChange} />
          <label htmlFor="topping1"> Cheese</label><br />

          <input type="checkbox" id="topping2" name="toppings" value="pepperoni" onChange={handleInputChange} />
          <label htmlFor="topping2"> Pepperoni</label><br />

          <input type="checkbox" id="topping3" name="toppings" value="mushrooms" onChange={handleInputChange} />
          <label htmlFor="topping3"> Mushrooms</label>
        </div>
        <br />

        <label>Best Stooge:</label><br />

        <div className='toppings-container'>
          <input type="radio" id="larry" name="stooge" value="Larry" onChange={handleInputChange} />
          <label htmlFor="larry"> Larry</label><br />

          <input type="radio" id="moe" name="stooge" value="Moe" onChange={handleInputChange} />
          <label htmlFor="moe"> Moe</label><br />

          <input type="radio" id="curly" name="stooge" value="Curly" onChange={handleInputChange} />
          <label htmlFor="curly"> Curly</label><br />
        </div>

        <label htmlFor="notes">Notes:</label><br />
        <textarea id="notes" name="notes" rows="4" cols="50" value={formState.notes} onChange={handleInputChange} className={errors.notes ? 'invalid' : ''}></textarea><br />

        <input type="submit" value="Submit" disabled={!formState.firstName || !formState.lastName || !formState.age || errors.notes} />
        <input type="reset" value="Reset" disabled={resetDisabled} onClick={handleReset} />

        <div id="output">
          Form State:
          <pre>{JSON.stringify(formState, null, 2)}</pre>
        </div>
      </form>
    </div>
  );
};

export default UserForm;
